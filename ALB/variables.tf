variable "region" {
  type = string
  default = "ap-southeast-2"
}

variable "dns_name" {
  type = string
}

variable "alb_subnets" {
  type = any
}

variable "alb_sg" {
  type = any
}

variable "vpc_id" {
  type = string
}

variable "instances" {
  type = any
}

variable "number_of_instances" {
  type = number
}