
data "aws_acm_certificate" "alb_ssl_certificate" {
  domain   = "*.mml.lab.myinstance.com"
  types    = ["AMAZON_ISSUED"]
}

resource "aws_alb" "alb" {
  name               = var.dns_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.alb_sg
  subnets            = var.alb_subnets

  enable_deletion_protection = false

    tags = {
    Environment = "production"
  }
}
resource "aws_lb_target_group" "web_target_group" {
  name     = "web-backend-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_lb_listener" "alb_listner_80" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "alb_listner_443" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = data.aws_acm_certificate.alb_ssl_certificate.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_target_group.arn
  }
}

resource "aws_alb_target_group_attachment" "attach-instance" {
  count = var.number_of_instances
  target_group_arn = aws_lb_target_group.web_target_group.arn
  target_id        = var.instances[count.index].id
  port             = 80
}