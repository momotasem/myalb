provider "aws" {
  region = var.region
}

module "networking" {
  source = ".//networking"
  vpc_cidr = var.vpc_cidr
  region = var.region
}

module "alb" {
  source = ".//ALB"
  region = var.region
  dns_name = var.dns_name
  alb_subnets = module.networking.public_subnets
  alb_sg = [module.networking.alb_sg]
  vpc_id = module.networking.vpc_id
  instances = module.ec2.instances
  number_of_instances = var.number_of_vms
  depends_on = [module.networking]
}

module "ec2" {
  source = ".//ec2"
  region = var.region
  ami_id = var.ami_id
  instance_type = var.instance_type
  vpc_id = module.networking.vpc_id
  sg = module.networking.webservers_sg
  number_of_instances = var.number_of_vms
  key_name = var.key_pair
  subnet_id = module.networking.private_subnets
}