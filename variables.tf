variable "region" {
  type = string
  default = "ap-southeast-2"
}

variable "ami_id" {
  type = string
  validation {
    # regex(...) fails if it cannot find a match
    condition     = can(regex("^ami-", var.ami_id))
    error_message = "The image_id value must be a valid AMI id, starting with \"ami-\"."
  }
}

variable "instance_type" {
  type = string
}

variable "number_of_vms" {
  type = number
}

variable "key_pair" {
  type = string
}

variable "dns_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}