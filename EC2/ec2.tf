
resource "aws_instance" "instances" {
  ami           = "ami-0aab712d6363da7f9"
  count = var.number_of_instances
  instance_type = var.instance_type
  key_name = var.key_name
  subnet_id = var.subnet_id[count.index]
  vpc_security_group_ids = [var.sg.id]
  tags = {
    Name = "Webserver-${count.index}"
  }
  user_data = <<-EOF
                #!/bin/bash
                sudo amazon-linux-extras install nginx1 -y
                sudo systemctl start nginx
                EOF
}

