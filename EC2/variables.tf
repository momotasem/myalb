variable "region" {
  type = string
  default = "ap-southeast-2"
}

variable "ami_id" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "sg" {
  type = any
}

variable "number_of_instances" {
  type = number
}

variable "key_name" {
  type = string
}

variable "subnet_id" {
  type = any
}