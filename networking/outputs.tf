output "public_subnets" {
  value = aws_subnet.public_subnet[*].id
}

output "private_subnets" {
  value = [for subnet in aws_subnet.private_subnet : subnet.id]
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "alb_sg" {
  value = aws_security_group.alb_sg.id
}

output "webservers_sg" {
  value = aws_security_group.webservers_sg
}