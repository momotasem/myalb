data "aws_availability_zones" "available" {}

locals {
  counter = length(data.aws_availability_zones.available.names)
}

## Create VPC ################
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "MML-VPC"
  }
}

## Create Public Subnet ################
resource "aws_subnet" "public_subnet" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.69.${10+count.index}.0/24"
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "Public_Subnet_${count.index}"
  }
}

## Create Private Subnets ################
resource "aws_subnet" "private_subnet" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = aws_vpc.vpc.id
  cidr_block = "10.69.${100+count.index}.0/24"
  availability_zone= data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false
  tags = {
    Name = "Private_Subnet_${count.index}"
  }
}

## Create IGW ################
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "IGW"
  }
}

## Public Route Table ################
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc.id
  route  {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.igw.id
    }
  depends_on = [aws_internet_gateway.igw]
  tags = {
    Name = "Public_Route_Table"
  }
}

## Private Route Table ################
resource "aws_route_table" "private_route_table" {
  count = local.counter
  vpc_id = aws_vpc.vpc.id
  route  {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway[count.index].id
  }
  depends_on = [aws_nat_gateway.nat_gateway]
  tags = {
    Name = "Private_Route_Table-${count.index}"
  }
}

## Public Subnets Association with Route Table ################
resource "aws_route_table_association" "public-subnet_association" {
  count = local.counter
  subnet_id = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

## Private Subnets Association with Route Table ################
resource "aws_route_table_association" "private-subnet_association" {
  count = local.counter
  subnet_id = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.private_route_table[count.index].id
}

resource "aws_eip" "nat_gateway_eip" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc      = true
  tags = {
    Name = "Nat-Gateway-${count.index}-eip_allocation"
  }
}

## Create Nat Gateway ################
resource "aws_nat_gateway" "nat_gateway" {
  count = "${length(data.aws_availability_zones.available.names)}"
  subnet_id     = aws_subnet.public_subnet[count.index].id
  allocation_id = aws_eip.nat_gateway_eip[count.index].id
  tags = {
    Name = "Nat-Gateway-${count.index}"
  }
  depends_on = [aws_internet_gateway.igw, aws_eip.nat_gateway_eip]
}

## Create ALB SG ################
resource "aws_security_group" "alb_sg" {
  name = "web-traffic-sg-80-443"
  description = "Allow web traffic 80-443"
  vpc_id = aws_vpc.vpc.id
  ingress  {
      description      = "443 from internet"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }
  ingress   {
      description      = "80 from internet"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  egress     {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  tags = {
    Name = "Allow web traffic"
  }
}

## Create Webservers SG ################
resource "aws_security_group" "webservers_sg" {
  name = "web-servers-traffic-80-443"
  description = "web-servers-traffic-80-443"
  vpc_id = aws_vpc.vpc.id
  ingress    {
      description      = "22 from internet"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }
  ingress    {
      description      = "80 from ALB"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      security_groups  = [aws_security_group.alb_sg.id]
    }
  ingress      {
      description      = "443 from ALB"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      security_groups  = [aws_security_group.alb_sg.id]
    }

  egress      {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

  tags = {
    Name = "Allow web traffic"
  }
}
