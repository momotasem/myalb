variable "region" {
  type = string
  default = "ap-southeast-2"
}

variable "vpc_cidr" {
  type = string
  validation {
    condition = contains(["10.69.0.0/16"],var.vpc_cidr)
    error_message = "The VPC CIDR should be 10.69.0.0/16."
  }
 }
